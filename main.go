package main

import (
	"github.com/rivo/tview"
)

var (
	nav = NewNavigator()
	win = &Window{
		App:    tview.NewApplication(),
		Layout: tview.NewFlex(),
		Nav:    nav,
	}
)

func main() {
	nav.InitPanels()

	win.Layout.
		AddItem(win.Nav.Layout, 0, 1, true).
		SetDirection(tview.FlexRow)

	win.App.
		SetRoot(win.Layout, true).
		SetFocus(nav.GetCurrentPanel().table)

	err := win.App.Run()
	if err != nil {
		panic(err)
	}
}
