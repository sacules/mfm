package main

import (
	"os"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
)

// FileCustomizer is the common interface for all types that want to
// customize the output of a Directory.
type FileCustomizer interface {
	Customize(files []os.FileInfo, table *tview.Table, columns map[string]int)
}

// FileCustomizerDefault is the default implementation for colorizing
// a given directory
type FileCustomizerDefault struct {
	ColorDir, ColorSymlink, ColorFile tcell.Color
}

// Customize the color of the file's names
func (defaultcustom *FileCustomizerDefault) Customize(files []os.FileInfo, table *tview.Table, columns map[string]int) {
	namecol := columns["Name"]

	for i, f := range files {
		cell := tview.NewTableCell(f.Name())

		if f.IsDir() {
			cell.SetTextColor(defaultcustom.ColorDir)
		} else if f.Mode()&os.ModeSymlink != 0 {
			cell.SetTextColor(defaultcustom.ColorSymlink)
		} else {
			cell.SetTextColor(defaultcustom.ColorFile)
		}

		cell.
			SetExpansion(1).
			SetAttributes(tcell.AttrBold)

		table.SetCell(i, namecol, cell)
	}
}

// DefaultCustomizer is a simple implementation of the FileCustomizer interface
var DefaultCustomizer = &FileCustomizerDefault{
	ColorDir:     tcell.ColorNavy,
	ColorSymlink: tcell.ColorTeal,
	ColorFile:    tcell.ColorWhite,
}
