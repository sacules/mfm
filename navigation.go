package main

import (
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/rivo/tview"
)

// Navigator comprehends the TUI that shows the current dir,
// and a collection of Directory pointers.
type Navigator struct {
	// Keep a very simple way to map a path to a Directory in order to
	// preserve focus and simplify navigation logic
	VisitedDirs map[string]*Directory

	// Use a grid to create a layout without gaps between panels and that
	// responds well to resizing of the terminal
	Layout *tview.Grid

	// Use a 3-paned look by default
	Panels []*Directory

	// Text area for file preview
	TextPreview *tview.TextView
}

// NewNavigator instantiates and returns a pointer to a Navigator
func NewNavigator() *Navigator {
	return &Navigator{
		VisitedDirs: make(map[string]*Directory),

		Layout: tview.NewGrid().
			SetBorders(true).
			SetColumns(0, 0, 0).
			SetRows(1, 0, 1),

		Panels: make([]*Directory, 3, 3),

		TextPreview: tview.NewTextView().SetWrap(false),
	}
}

// InitPanels takes care of creating the panels properly. This is done
// outside NewNavigator in order to avoid an initialization loop.
func (nav *Navigator) InitPanels() {
	var columns = map[string]int{
		"Name": 0,
	}

	// Get the dir we are in
	cwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	// Central panel
	currpanel := NewDirectory(cwd, columns)
	currpanel.PrintFiles()
	nav.Layout.AddItem(currpanel.table, 1, 1, 1, 1, 1, 1, false)
	nav.Panels[1] = currpanel
	nav.VisitedDirs[cwd] = currpanel

	// Don't add a previous panel if we're at the top
	if cwd != "/" {
		updir, err := filepath.Abs("..")
		if err != nil {
			panic(err)
		}

		prevpanel := NewDirectory(updir, columns)
		prevpanel.PrintFiles()
		nav.Layout.AddItem(prevpanel.table, 1, 0, 1, 1, 1, 1, false)
		nav.Panels[0] = prevpanel
		nav.VisitedDirs[updir] = prevpanel
	}

	// Show a preview of the next directory
	curritem := currpanel.GetCurrentFileName()
	nextpanel := NewDirectory(curritem, columns)
	if nextpanel != nil {
		nextpanel.PrintFiles()
		nav.Layout.AddItem(nextpanel.table, 1, 2, 1, 1, 1, 1, false)
		nav.Panels[2] = nextpanel

		// Save the path to the navigator's map
		path, err := filepath.Abs(curritem)
		if err != nil {
			panic(err)
		}

		nav.VisitedDirs[path] = nextpanel
	} else {
		// nav.Layout.RemoveItem(nav.Panels[2].table)
		nav.Panels[2] = nil
		nav.Layout.AddItem(nav.TextPreview, 1, 2, 1, 1, 1, 1, false)

		cmd := exec.Command("file", curritem)
		output, err := cmd.Output()
		if err != nil {
			return
		}

		if strings.Contains(string(output), "text") {
			data, err := ioutil.ReadFile(curritem)
			if err != nil {
				nav.TextPreview.Clear()
			} else {
				nav.TextPreview.SetText(string(data))
			}
		} else {
			nav.TextPreview.SetText(string(output))
		}
	}
}

// NextPanelUpdate is called each time the user moves to another row in a
// Directory
func (nav *Navigator) NextPanelUpdate(row int) {
	defer win.App.ForceDraw()

	currpanel := nav.GetCurrentPanel()
	currfile := currpanel.GetCurrentFileName(row)
	fi := currpanel.GetFileInfo(currfile)

	if fi.IsDir() {
		abspath, err := filepath.Abs(currfile)
		if err != nil {
			panic(err)
		}

		// Check if current path has been cached
		nextpanel, ok := nav.VisitedDirs[abspath]
		if !ok {
			nextpanel = NewDirectory(abspath, currpanel.Columns)

			// Sometimes we can't access the dir, so let's check for it
			// TODO: modify NewDirectory to return an err
			if nextpanel == nil {
				nav.Layout.RemoveItem(nav.Panels[2].table)
				nav.Panels[2] = nil
				return
			}
			nav.VisitedDirs[abspath] = nextpanel
		}

		// Update file list
		nextpanel.PrintFiles()

		// Replace old panel with new one
		if nav.Panels[2] != nil {
			nav.Layout.RemoveItem(nav.Panels[2].table)
		}

		nav.Layout.AddItem(nextpanel.table, 1, 2, 1, 1, 1, 1, false)
		nav.Panels[2] = nextpanel
	} else {
		if nav.Panels[2] != nil {
			nav.Layout.RemoveItem(nav.Panels[2].table)
		}

		nav.Panels[2] = nil
		nav.Layout.AddItem(nav.TextPreview, 1, 2, 1, 1, 1, 1, false)

		cmd := exec.Command("file", fi.Name())
		output, err := cmd.Output()
		if err != nil {
			return
		}

		if strings.Contains(string(output), "text") {
			data, err := ioutil.ReadFile(fi.Name())
			if err != nil {
				nav.TextPreview.Clear()
			} else {
				nav.TextPreview.
					SetWrap(false).
					SetText(string(data))
			}
		} else {
			nav.TextPreview.
				SetWordWrap(true).
				SetText(string(output))
		}
	}
}

// GetCurrentPanel returns a pointer to the current panel in focus
func (nav *Navigator) GetCurrentPanel() *Directory {
	return nav.Panels[1]
}

func (nav *Navigator) goUp() {
}

func (nav *Navigator) goDown() {
}
