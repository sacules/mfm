module gitlab.com/sacules/mfm

require (
	github.com/gdamore/tcell v1.1.2
	github.com/rivo/tview v0.0.0-20190515161233-bd836ef13b4b
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f // indirect
	golang.org/x/net v0.0.0-20190514140710-3ec191127204 // indirect
	golang.org/x/sys v0.0.0-20190515190549-87c872767d25 // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190515235946-4f9510c6a12d // indirect
)
