package main

import (
	"github.com/rivo/tview"
)

// Window controls the whole TUI, and holds the main widgets
type Window struct {
	App    *tview.Application
	Layout *tview.Flex
	Title  *tview.Primitive
	Nav    *Navigator
	Status *tview.Primitive
}
