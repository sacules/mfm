package main

import (
	"io/ioutil"
	"os"
	"strings"

	"github.com/rivo/tview"
)

// Directory represents the content of a directory and
// its visual representation
type Directory struct {
	Files       []os.FileInfo
	Columns     map[string]int
	Customizers []FileCustomizer
	ShowHidden  bool

	table *tview.Table
}

// NewDirectory gets the files in the given path, installs the given
// columns and customizers, and returns a pointer to Directory. Returns
// nil if the path couldn't be read.
func NewDirectory(path string, cols map[string]int, customs ...FileCustomizer) *Directory {
	var (
		dir = &Directory{
			Columns:    cols,
			ShowHidden: false,
		}

		filecustoms []FileCustomizer

		table = tview.NewTable()

		files, err = ioutil.ReadDir(path)
	)

	if err != nil {
		return nil
	}
	dir.Files = files

	if len(customs) > 0 {
		filecustoms = customs
	} else {
		filecustoms = append(filecustoms, DefaultCustomizer)
	}
	dir.Customizers = filecustoms

	table.
		SetSelectable(true, false).
		SetSelectionChangedFunc(func(row, column int) {
			nav.NextPanelUpdate(row)
		})
	dir.table = table

	return dir
}

// PrintFiles writes the names of the files on the table. If ShowHidden
// is true, it will print everything. Uses the customizers.
func (d *Directory) PrintFiles() {
	var files []os.FileInfo

	if d.ShowHidden {
		files = d.Files
	} else {
		for _, f := range d.Files {
			if !strings.HasPrefix(f.Name(), ".") {
				files = append(files, f)
			}
		}
	}

	for _, custom := range d.Customizers {
		custom.Customize(files, d.table, d.Columns)
	}
}

// GetCurrentFileName checks for the selected item, and column that
// contains its name.
func (d *Directory) GetCurrentFileName(row ...int) string {
	var r int

	if len(row) != 1 {
		r, _ = d.table.GetSelection()
	} else {
		r = row[0]
	}

	currcol := d.Columns["Name"]

	return d.table.GetCell(r, currcol).Text
}

// GetFileInfo gets the corresponding FileInfo to the givne name from
// the the Directory's Files.
func (d *Directory) GetFileInfo(name string) os.FileInfo {
	for _, f := range d.Files {
		if f.Name() == name {
			return f
		}
	}

	return nil
}
